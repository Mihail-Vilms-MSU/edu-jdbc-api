package dao;

import java.sql.*;

public class DbAdapter {
    // 01 - Variables
    String url = "jdbc:postgresql://127.0.0.1:5432/orders-service-tutorial-db";
    String username = "postgres";
    String password = "Covfefe1";

    // 02 - Database variables
    Connection connection = null;
    Statement statement = null;
    ResultSet resultset = null;

    // 03 - Connection
    public DbAdapter(){
    }

    /**
     * Connect to database
     */
    public void connect(){
        try {
            // connect to database
            connection = DriverManager.getConnection(url, username, password);
            // Print success
            System.out.println("DB connection established");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void disconnect(){
        try {
            if (statement != null){
                statement.close();
            }
            if (resultset != null){
                resultset.close();
            }
            if (connection != null){
                connection.close();
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
    }
}
