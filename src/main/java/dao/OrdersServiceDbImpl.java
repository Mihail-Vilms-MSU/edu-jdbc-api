package dao;

import domain.Product;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrdersServiceDbImpl extends DbAdapter{
    public void createTables() {
        try {
            statement = connection.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS products (" +
                    "product_id SERIAL PRIMARY KEY NOT NULL, " +
                    "product_name VARCHAR(255) NOT NULL, " +
                    "product_price REAL)";

            statement.executeUpdate(sql);
            statement.close();
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void setup() {
        // First count of rows
        int numberOfRaws = -1;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT count (*) from products");
            ResultSet ruleSet = statement.executeQuery();
            while(ruleSet.next()){
                numberOfRaws = ruleSet.getInt(1);
            }

        } catch (SQLException e){
            e.printStackTrace();
        }

        if (numberOfRaws == 0){
            // Do setup
            System.out.println("Number of rows: " + numberOfRaws);

            insertProduct("Espresso", 11.22);
            insertProduct("Espresso1", 44.22);
            insertProduct("Espresso2", 55.22);
        }
    }

    public void insertProduct(String productName, Double productPrice){
        System.out.println("Insert product: " + productName + "; with prices: " + productPrice);
        try {

            PreparedStatement statement = connection.prepareStatement("INSERT INTO products (" +
                    "product_name, product_price)" +
                    "VALUES(?, ?)");
            statement.setString(1, productName);
            statement.setDouble(2, productPrice);
            statement.executeUpdate();
            statement.close();

        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    public List selectAllProducts(){
        List<Product> allProductsList = new ArrayList<Product>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT product_id, product_name, product_price FROM products");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long productId = resultSet.getLong(1);
                String productName = resultSet.getString(2);
                Double productPrice = resultSet.getDouble(3);

                Product product = new Product(productId, productName, productPrice);
                allProductsList.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return allProductsList;
    }

    public void updateProduct(long productId, String productName, Double productPrice){
        System.out.println("Updating item number " + productId +
                " with Name = " + productName + " price = " + productPrice);

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE products SET product_name=?," +
                    "product_price=? WHERE product_id=?");

            preparedStatement.setString(1, productName);
            preparedStatement.setDouble(2, productPrice);
            preparedStatement.setLong(3, productId);
            preparedStatement.executeUpdate();
            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteProduct(long productId){
        System.out.println("Deleting producting number " + productId);
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM products WHERE product_id=?");
            preparedStatement.setLong(1, productId);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
