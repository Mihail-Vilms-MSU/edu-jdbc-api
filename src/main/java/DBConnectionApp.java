import dao.OrdersServiceDbImpl;
import domain.Product;

import java.util.ArrayList;
import java.util.List;

public class DBConnectionApp {
    public static void main(String[] args){
        OrdersServiceDbImpl ordersServiceDb = new OrdersServiceDbImpl();

        // Connect
        ordersServiceDb.connect();

        // Create table
        ordersServiceDb.createTables();

        // Run setup
        ordersServiceDb.setup();

        // Insert
        // ------

        // Retrieving data
        List<Product> allProducts = ordersServiceDb.selectAllProducts();
        for (Product product : allProducts){
            System.out.println("ID = " + product.getId());
            System.out.println("Name = " + product.getName());
            System.out.println("Price = " + product.getPrice());
        }

        //Update one product
        ordersServiceDb.updateProduct(2, "Product2NEW", 100.00);

        // Deleting product
        ordersServiceDb.deleteProduct(2);

        // Disconnect from db
        ordersServiceDb.disconnect();
    }
}
